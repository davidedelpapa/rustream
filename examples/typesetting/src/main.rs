use rustream::prelude::*;

fn main() {
    yew::Renderer::<App>::new().render();
}

#[function_component(App)]
pub fn app() -> Html {
    let title = "My title!";
    html! {
        <Page>
            <Toc />
            // <Title class={"heading"} data={title} changehead={false} />
            <Title data={title} />
            <Header data={"Title 1"} id={"title1".to_string()}/>
            <Header data={"Title 2"} />
            <Header data={"Title 3"} />
            <Header data={"Title 4"} id={"title4".to_string()}/>
            <Header data={"Title 5"} />
            <Header data={"Title 6"} />
            <Header data={"Title 7"} />
            <Header data={"Title 8"} />
            <Header data={"Title 9"} />
            <Header data={"Title 10"} id={"title10".to_string()} />
        </Page>
    }
}