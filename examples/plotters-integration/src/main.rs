use rustream::prelude::*;
use plotters::prelude::*;
use plotters_canvas::CanvasBackend;

fn main() {
    yew::Renderer::<App>::new().render();
}

#[function_component(App)]
pub fn app() -> Html {
    let callback = |id: String|{
        // The following code is adapted from plotters' own quickstart example
        let root = CanvasBackend::new("mycanvas").expect(id.as_str()).into_drawing_area();

        root.fill(&WHITE).unwrap();
        let mut chart = ChartBuilder::on(&root)
            .caption("y=x^2", ("sans-serif", 50).into_font())
            .margin(5)
            .x_label_area_size(30)
            .y_label_area_size(30)
            .build_cartesian_2d(-1f32..1f32, -0.1f32..1f32).unwrap();

        chart.configure_mesh().draw().unwrap();

        chart
            .draw_series(LineSeries::new(
                (-50..=50).map(|x| x as f32 / 50.0).map(|x| (x, x * x)),
                &RED,
            )).unwrap()
            .label("y = x^2")
            .legend(|(x, y)| PathElement::new(vec![(x, y), (x + 20, y)], &RED));

        chart
            .configure_series_labels()
            .background_style(&WHITE.mix(0.8))
            .border_style(&BLACK)
            .draw().unwrap();

        root.present().unwrap();

    };
    html! {
        <Canvas id={"mycanvas"} width={"640"} height={"480"}>
            <CanvasElement {callback} />
        </Canvas>
    }
}
