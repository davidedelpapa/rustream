// Checked with poloto = "17.1.0"
use rustream::prelude::*;

fn main() {
    yew::Renderer::<App>::new().render();
}

#[function_component(App)]
pub fn app() -> Html {
    // The following is adapted from poloto's own barchart example
    // https://github.com/tiby312/poloto/blob/master/examples/bar.rs

    let data = [
        (20, "potato"),
        (14, "broccoli"),
        (53, "pizza"),
        (30, "avocado"),
    ];

    let plt = poloto::build::bar::gen_simple("", data, [0])
        .label(("Comparison of Food Tastiness", "Tastiness", "Foods"))
        .append_to(poloto::header().light_theme())
        .render_string().unwrap();
    
    html! {
        <>
            <h1>{ "Poloto Integration" }</h1>
            <RawSvg data={plt} />
        </>
    }
}
