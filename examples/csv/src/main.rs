use rustream::prelude::*;
use gloo_console::log;

fn main() {
    yew::Renderer::<App>::new().render();
}

#[function_component(App)]
pub fn app() -> Html {
    let title = "Hello, CSV!";
    let csv = 
r#"name,age
David,37
Stephanie,31"#;
    html! {
        <>
            <LoadStyles />
            <div class={"content"}>
                <Csv data={ csv }>
                    <h2>{&title}</h2>
                    <ViewCsv />
                    <ViewCsvAlt />
                </Csv>
            </div>
        </>
    }
}

 #[function_component(ViewCsvAlt)]
 pub fn view_csv_alt() -> Html {
    let csv = use_context::<CsvContext>().expect("no ctx found");
    // This one uses the standard formatter for DTable; quick&dirty
    let data = format!("{}", csv.data);
    log!(&data);
    html! { 
        <pre>
        { data }
        </pre> }
 }
 