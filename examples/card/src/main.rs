use rustream::prelude::*;

fn main() {
    yew::Renderer::<App>::new().render();
}

#[function_component(App)]
pub fn app() -> Html {
    html! {
        <>
            <LoadStyles />
            <Card>
                <h2>{ "Hello Card!" }</h2>
                <p>{ "This is a " }<a href={"#cards2"}>{ "link to Cards n.2 section" }</a></p>
            </Card>
            <div class= {"row"}>
                <div class= {"column"}>
                    <Card shadow={true} >
                        <p>{ "Card 1" }</p>
                    </Card>
                </div>
                <div class= {"column"}>
                    <Card shadow={true}>
                        <p>{ "Card 2" }</p>
                    </Card>
                </div>
            </div>

            <div class={"row"}>
                <div class= {"column"}>
                    <Card class= {"color1"}>
                        <CardHeader class= {"color1"}>{ "Card 1" }</CardHeader>
                        <CardBody class= {"color1"}>
                            <i class={"material-icons"}>{"face"}</i>
                            <p>{ "Card 1" }</p>
                        </CardBody>
                    </Card>
                </div>
                <div class= {"column"}>
                    <Card class= {"color2"}>
                        <CardHeader class= {"color2"}>{ "Card 2" }</CardHeader>
                        <CardBody class= {"color2"}>
                            <i class={"material-icons"}>{"diamond"}</i>
                            <p>{ "Card 2" }</p>
                        </CardBody>
                    </Card>
                </div>
                <div class= {"column"}>
                    <Card class= {"color3"}>
                        <CardHeader class= {"color3"}>{ "Card 3" }</CardHeader>
                        <CardBody class= {"color3"}>
                            <i class={"material-icons"}>{"forest"}</i>
                            <p>{ "Card 3" }</p>
                        </CardBody>
                    </Card>
                </div>
                <div class= {"column"}>
                    <Card class= {"color4"}>
                        <CardHeader class= {"color4"}>{ "Card 4" }</CardHeader>
                        <CardBody class= {"color4"}>
                            <i class={"material-icons"}>{"thumb_up"}</i>
                            <p>{ "Card 4" }</p>
                        </CardBody>
                    </Card>
                </div>  
            </div>

            <Section name="cards2" class={"row"}>
                <Card class={"color2 column"} shadow={true}>
                    <CardHeader class= {"color2"}>{ "Card 1" }</CardHeader>
                    <CardBody class= {"color2"}>
                        <i class={"material-icons"}>{"people"}</i>
                        <p>{ "Card 1" }</p>
                    </CardBody>
                </Card>
                <Card class={"color4 column"} shadow={true}>
                    <CardHeader class= {"color4"}>{ "Card 2" }</CardHeader>
                    <CardBody class= {"color4"}>
                        <i class={"material-icons"}>{"poll"}</i>
                        <p>{ "Card 2" }</p>
                    </CardBody>
                </Card>
                <Card class={"color1 column"} shadow={true}>
                    <CardHeader class= {"color1"}>{ "Card 3" }</CardHeader>
                    <CardBody class= {"color1"}>
                        <i class={"material-icons"}>{"share"}</i>
                        <p>{ "Card 3" }</p>
                    </CardBody>
                </Card>
                <Card class= {"color3 column"} shadow={true}>
                    <CardHeader class= {"color3"}>{ "Card 4" }</CardHeader>
                    <CardBody class= {"color3"}>
                        <i class={"material-icons"}>{"room_service"}</i>
                        <p>{ "Card 4" }</p>
                    </CardBody>
                </Card> 
            </Section>

            <Section name={"lastSection"} shadow={false}>
                <h2>{ "Last section"} </h2>
                <p>{ "Last section description" }</p>
            </Section>
        </>
    }
}