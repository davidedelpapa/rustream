use rustream::prelude::*;

fn main() {
    yew::Renderer::<App>::new().render();
}

#[function_component(App)]
pub fn app() -> Html {
    let title = "Hello, Markdown!";
    let code = r#"
```Rust
fn main(){
    println!("Hello, World!");
}
```
"#;
    html! {
        <div>
            <h2 class={"heading"}>{&title}</h2>
            <ul>
            <li><Markdown data={"_Italic_".to_string()} /></li>
            <li><Markdown data={"__Bold__".to_string()} /></li>
            
            // Strikethrough is enabled by default, 
            // therefore `strikethrough={true}` could be omitted
            <li><Markdown data={"~~Striketrhough~~".to_string()} strikethrough={true} /></li>
            
            <li><Markdown data={"`Inline code`".to_string()} /></li>
            </ul>
            <h3> { "Fenced code-block" } </h3>
            <Markdown data={ code } />
        </div>
    }
}