use rustream::prelude::*;
use ndarray::prelude::*;
use gloo_console::log;

fn main() {
    yew::Renderer::<App>::new().render();
}

#[function_component(App)]
pub fn app() -> Html {
    let title = "Hello, ndarray!";
    let arr = array![[1.,2.,3.], [4.,5.,6.]];
    let t = DTable::from_ndarray(arr);
    let data = format!("{}", t);
    log!(&data);

    html! {
        <>
            <LoadStyles />
            <div class={"content"}>
                <h2>{&title}</h2>
                <pre>
                    { data }
                </pre>
                <ViewTable data={t} />
            </div>
        </>
    }
}
 