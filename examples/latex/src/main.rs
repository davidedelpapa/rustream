use rustream::prelude::*;

fn main() {
    yew::Renderer::<App>::new().render();
}

#[function_component(App)]
pub fn app() -> Html {
    let relativity = "E = mc^2".to_string();
    let f = r#"
    \begin{align*}
    f(x) &= x^2\\
    g(x) &= \frac{1}{x}\\
    F(x) &= \int^a_b \frac{1}{3}x^3
    \end{align*}
    "#.to_string();
    html! {
        <div>
            <h2>{ "Hello LaTeX" }</h2>
            <Latex data={ relativity } />
            <Latex data={ f } display={true} /> //>NOTE: align* needs the display environment to render
        </div>
    }
}