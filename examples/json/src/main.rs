use rustream::prelude::*;

fn main() {
    yew::Renderer::<App>::new().render();
}

#[function_component(App)]
pub fn app() -> Html {
    let title = "Hello, JSON!";
    let json_data = r#"
        {
            "name": "John Doe",
            "age": 43,
            "phones": [
                "+44 1234567",
                "+44 2345678"
            ]
        }"#;
    html! {
        <Json data={ json_data }>
            <h2 class={"heading"}>{&title}</h2>
            <ViewJson />
            <ViewString /> //Implemented here below
        </Json>
    }
}

#[function_component(ViewString)]
pub fn view_string() -> Html {
    html! {
        <>
        <DataView<String> data={ "json".to_string() } wrapper={DataViewWrapper::Div} id={"MyDiv".to_string()}/>
        </>
    }
}