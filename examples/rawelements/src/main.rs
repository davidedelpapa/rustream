use rustream::prelude::*;


fn main() {
    yew::Renderer::<App>::new().render();
}

#[function_component(App)]
pub fn app() -> Html {
    
    html! {
        <>
            <RawHtml data={"<h3>Blue Circle</h3>".to_string()} />
            <RawSvg data={r#"
                <svg height="130" width="130">
                    <circle cx="45" cy="45" r="35" stroke="black" stroke-width="1" fill="blue" />
                    Currently your browser does not support inline SVG.  
                </svg> 
            "#} />
        </>
    }
}
