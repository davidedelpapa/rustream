# Rustream
_Rust data apps made easy._


This crate provides components that extend [`Yew`], 
in order to create beautiful Data Apps.

## Extended Yew Components
These components must be used in a Yew `html!` macro.

```Rust
use rustream::prelude::*;

fn main() {
    yew::Renderer::<App>::new().render();
}

#[function_component(App)]
pub fn app() -> Html {

    html! {
        <Markdown data={"_Italic_".to_string()} />
    }
}
```

In order to generate smaller `.wasm` files, most of these components are feature-gated.
See the corresponding *features* section.

> **Note about output file size**: running `trunk build --release` diminishes greatly the wasm file output. Unless debug is needed, in production the files must be generated thorugh the `release` flag.

### Display text and structures

- [`Markdown`] The `<Markdown>` component [`markdown` and `default` features]
- [`RawHtml`] The `<RawHtml>` component
- [`RawSvg`] The `<RawSvg>` component
- [`DataView`] The `<DataView>` component and derivatives (Some derivate components will work only when the corresponding features are enabled)
- [`Latex`] The `<Latex>` component [`latex` feature]
### Data contexts

- [`Json`] The `<Json>` context [`json` and `default` features]
- [`Css`] The `<Css>` context [`csv` feature]
- [`Canvas`] The `<Canvas>` context 
- [`DTable`](components/table/index.html) The `DTable` and `DColumn` data-structures [`table` and `default` features]
### Typesetting module

The elements in the typesetting module are best suited to be displayed in a [`Page`] component (see the Layout module below). In this case see also the [`Toc`] component to generate a table of content.

- [`Title`] The `<Title>` component 
- [`Header`] The `<Header>` component

### Layout module

Components will not work properly without the `style` (or `default`) feature. 

However, CSS stylesheets could be loaded with a `<link>` tag in the *index.html* 
(see the [`Style guide`](components/style/index.html) for more details).

See the [`LoadStyles`] in the Global section below, to load a default CSS module that "just works" for *Rustream*.

- [`NavBar`] The `<NavBar>` component 
- [`Logo`] The `<Logo>` component 
- [`NavMenu`] The `<NavMenu>` component 
- [`Card`] The `<Card>`, `<CardHeader>`, and `<CardBody>` components
- [`Section`] The `<Section>` component
- [`Page`] The `<Page>` component
- [`Toc`] The `<Toc>` component
### Media
- [`Canvas`] The `<Canvas>` and `<CanvasElement>` components

### Global
- [`LoadStyles`] The `<LoadStyles>` component

## Features
Features available to declare in the *Cargo.toml* imports:

- `defualt`: load the `markdown`, `style`, `json`, and `table` features
- `all`: load all features available
- `csv`: used for the `<Csv>` context
- `json`: used for the `<Csv>` context, and `DTable` data-structure
- `latex`: used for the `<Latex>` component
- `markdown`: used for the `<Markdown>` component
- `style`: used for all styled componet (see [`LoadStyles`] and [`Layout`] modules)
- `table`: used for the `DTable` data-structure 

## Issues and Further Improvements
Right now the generated wasm files are too big, even using the `--release` flag in `trunk build`; those in the examples are in the range of 3 to 4 Mb each without the `release` flag.

It may depend also on the CSS styles that right now are copied raw with the *stylist* crate.
A better and size-friendly approach is warranted and will be one of the first issues to tackle in the near future.

## Inspiration
Rustream was inspired by [`Streamlit`].

## License

Individual components inside the *vendor* folder have their own license, which you can find alongside the code.

(C) 2022, Davide Del Papa, under MIT License terms (see LICENSE file).

[`Yew`]: https://yew.rs/
[`Streamlit`]: https://streamlit.io/
[`Markdown`]: https://codeberg.org/davidedelpapa/rustream/src/branch/main/src/components/markdown.rs
[`RawHtml`]: https://codeberg.org/davidedelpapa/rustream/src/branch/main/src/components/rawhtml.rs
[`RawSvg`]: https://codeberg.org/davidedelpapa/rustream/src/branch/main/src/components/rawhtml.rs
[`Json`]: https://codeberg.org/davidedelpapa/rustream/src/branch/main/src/components/json.rs
[`Css`]: https://codeberg.org/davidedelpapa/rustream/src/branch/main/src/components/json.rs
[`DataView`]: https://codeberg.org/davidedelpapa/rustream/src/branch/main/src/components/dataview.rs
[`Latex`]: https://codeberg.org/davidedelpapa/rustream/src/branch/main/src/components/latex.rs
[`Title`]: https://codeberg.org/davidedelpapa/rustream/src/branch/main/src/components/typesetting.rs
[`LoadStyles`]: https://codeberg.org/davidedelpapa/rustream/src/branch/main/src/components/style.rs
[`NavBar`]: https://codeberg.org/davidedelpapa/rustream/src/branch/main/src/components/layout.rs
[`Logo`]: https://codeberg.org/davidedelpapa/rustream/src/branch/main/src/components/layout.rs
[`NavMenu`]: https://codeberg.org/davidedelpapa/rustream/src/branch/main/src/components/layout.rs
[`DTable`]: components/table/index.html
[`Card`]: https://codeberg.org/davidedelpapa/rustream/src/branch/main/src/components/layout.rs
[`Section`]: https://codeberg.org/davidedelpapa/rustream/src/branch/main/src/components/layout.rs
[`Layout`]: https://codeberg.org/davidedelpapa/rustream/src/branch/main/src/components/layout.rs
[`Canvas`]: https://codeberg.org/davidedelpapa/rustream/src/branch/main/src/components/canvas.rs
[`Page`]: https://codeberg.org/davidedelpapa/rustream/src/branch/main/src/components/layout.rs
[`Toc`]: https://codeberg.org/davidedelpapa/rustream/src/branch/main/src/components/layout.rs
[`Header`]: https://codeberg.org/davidedelpapa/rustream/src/branch/main/src/components/typesetting.rs