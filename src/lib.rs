//! Rustream, Rust data apps made easy.
//! 
//! This crate provides components that extend [`Yew`], 
//! in order to create beautiful Data Apps.
//! 
//! # Extended Yew Components
//! These components must be used in a Yew `html!` macro.
//! 
//! ```Rust
//! use rustream::prelude::*;
//!
//! fn main() {
//!     yew::start_app::<App>();
//! }
//! 
//! #[function_component(App)]
//! pub fn app() -> Html {
//! 
//!     html! {
//!         <Markdown data={"_Italic_".to_string()} />
//!     }
//! }
//! ```
//! ## Components
//! ### Display text
//! - [`Markdown`](components/markdown/index.html) The `<Markdown>` component [`markdown` and `default` features]
//! - [`RawHtml`](components/rawhtml/index.html) The `<RawHtml>` component
//! - [`DataView`](components/dataview/index.html) The `<DataView>` component and derivatives (Some derivate components will work only when the corresponding features are enabled)
//! - [`Latex`](components/latex/index.html) The `<Latex>` component [`latex` feature]
//! 
//! ### Data contexts and structures
//! - [`Json`](components/json/index.html) The `<Json>` context [`json` and `default` features]
//! - [`Csv`](components/csv/index.html) The `<Csv>` context [`csv` feature]
//! - [`Canvas`](components/canvas/index.html) The `<Canvasv>` context, helpfull to draw charts
//! - [`DTable`](components/table/index.html) The `DTable` and `DColumn` data-structures [`table` and `default` features]
//! 
//! ### Typesetting module
//! - [`Title`](components/typesetting/index.html) The `<Title>` component
//! 
//! ### Layout module
//! Components will not work properly without the `style` (or `default`) feature. 
//! 
//! However, CSS stylesheets could be loaded with a `<link>` tag in the *index.html* 
//! (see the [`Style guide`](components/style/index.html) for more details).
//! 
//! - [`NavBar`](components/layout/index.html) The `<NavBar>` component
//! - [`Logo`](components/layout/index.html) The `<Logo>` component
//! - [`NavMenu`](components/layout/index.html) The `<NavMenu>` component
//! - [`Card`](components/layout/index.html) The `<Card>` component
//! - [`CardHeader`](components/layout/index.html) The `<CardHeader>` component
//! - [`CardBody`](components/layout/index.html) The `<CardBody>` component
//! - [`Section`](components/layout/index.html) The `<Section>` component
//! 
//! ### Global
//! - [`LoadStyles`](components/style/index.html) The `<LoadStyles>` component [`style` and `default` features]
//! 
//! ## Inspiration
//! Rustream was inspired by [`Streamlit`].
//! 
//! ## License
//! (C)Davide Del Papa 2022, under the MIT license terms and conditions.
//! 
//! [`Yew`]: https://yew.rs/
//! [`Streamlit`]: https://streamlit.io/
#![warn(missing_docs)]

/// Components available for use in Yew's `html!` macro
pub mod components;
/// Data structures available for use in Rustream
pub mod structures;

pub mod prelude {
    //! The Rustream Prelude
    //!
    //! The purpose of this module is to easy out imports of many common types:
    //!
    //! ```
    //! # #![allow(unused_imports)]
    //! use rustream::prelude::*;
    //! ```
    //! 
    //! This module conveniently exports Yew's own prelude, as well as other crates needed parts.
    #[cfg(feature = "markdown")]
    pub use crate::components::markdown::*;
    pub use crate::components::rawhtml::*;
    #[cfg(feature = "json")]
    pub use crate::components::json::*;
    pub use crate::components::dataview::*;
    #[cfg(feature = "latex")]
    pub use crate::components::latex::*;
    pub use crate::components::typesetting::*;
    #[cfg(feature = "style")]
    pub use crate::components::style::*;
    pub use crate::components::layout::*;
    #[cfg(feature = "csv")]
    pub use crate::components::csv::*;
    #[cfg(feature = "table")]
    pub use crate::structures::table::*;
    #[cfg(feature = "table")]
    pub use crate::structures::column::*;
    pub use crate::components::canvas::*;

    // Re-exports Yew
    #[doc(hidden)]pub use yew::AppHandle;
    #[doc(hidden)]pub use yew::callback::Callback;
    #[doc(hidden)]pub use yew::context::{ContextHandle, ContextProvider};
    #[doc(hidden)]pub use yew::events::*;
    #[doc(hidden)]pub use yew::functional::*;
    #[doc(hidden)]pub use yew::html::{
        create_portal, BaseComponent, Children, ChildrenWithProps, Classes, Component, Context,
        Html, HtmlResult, NodeRef, Properties,
    };
    #[doc(hidden)]pub use yew::macros::{classes, html, html_nested};
    #[doc(hidden)]pub use yew::suspense::Suspense;
    #[doc(hidden)]pub use yew::virtual_dom::AttrValue;
    #[doc(hidden)]pub use yew::{set_custom_panic_hook, Renderer};

    // Re-Export stylist
    #[cfg(feature = "style")]
    #[doc(hidden)]pub use stylist::yew::styled_component;
}
pub use self::prelude::*;