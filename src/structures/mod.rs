/// The *table* module, containing tabular dataset that mimicks a DataFrame.
#[cfg(feature = "table")]
pub mod table;

/// The DColumn, used in DTable
#[cfg(feature = "table")]
pub mod column;