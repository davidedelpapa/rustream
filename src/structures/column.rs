use serde_json::value::Value;
use serde_json::json;

/// DColumn structure
// @TODO maybe private fields, make getters/setters
#[derive(Debug,Clone,PartialEq)]
pub struct DColumn {
    /// DColumn Title
    pub title: String,
    /// DColumn vector of cells
    pub cells: Vec<Value>
}
impl DColumn{
    /// DColumn constructor; it requires a title
    pub fn new(title: String) -> DColumn {
        DColumn {
            title,
            cells: Vec::new(),
        }
    }

    /// DColumn constructor; it does not require a title
    pub fn new_column() -> DColumn {
        DColumn {
            title: "".to_string(),
            cells: Vec::new(),
        }
    }

    /// Creates a new column from a generic array
    #[cfg(feature = "serde")]
    pub fn from_array<A>(arr: &[A]) -> DColumn 
    where 
        A: Clone + serde::ser::Serialize 
    {
        let a = arr.to_vec();
        let cells = a.iter().map(|e| {
            json!(e)
        }).collect();
        DColumn {
            title: "".to_string(),
            cells,
        }
    }

    /// Set the DColumn title
    pub fn set_title(&mut self, title: String){
        self.title = title;
    }

    /// Push a new `Value` in the column
    pub fn push<T>(&mut self, val: T)
    where
        T: serde::Serialize
    {
        self.cells.push(json!(val));
    }
}