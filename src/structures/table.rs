/* Roadmap:
- [x] Make data structure
- [x] Document data structure
- [ ] Unit Test data structure
- [ ] Improve data structure, following the TODOs
 */
//! # Description
//! A data structure that mimicks a DataFrame, useful to exchange data in a tabular format
//! 
//! At the core, DTable and DColumn use [`serde_json::Value`]. 
//! It stores each `value` is a column (DColumn, see); the table itself is a collection of columns.
//! 
//! [`serde_json::Value`]: https://docs.serde.rs/serde_json/value/enum.Value.html
// @TODO Unit test this module
use std::fmt;
use crate::prelude::*;
use serde_json::value::Value;
//use gloo_console::log;

/// DTable structure
// @TODO maybe private fields, make getters/setters
#[derive(Debug,Clone,PartialEq)]
pub struct DTable {
    /// DTable Title
    pub title: String,
    /// DTable vector of columns
    pub columns: Vec<DColumn>,
}
impl DTable {
    /// DTable constructor; it requires a title
    pub fn new(title: String) -> DTable {
        DTable {
            title,
            columns: Vec::new(),
        }
    }

    /// DTable constructor; it does not require a title
    pub fn new_table() -> DTable {
        DTable {
            title: "".to_string(),
            columns: Vec::new(),
        }
    }

    /// Create a DTable from a ndarray::Array2
    #[cfg(feature = "ndarray")]
    pub fn from_ndarray<T>(arr: ndarray::Array2<T>)->DTable 
    where
        T: Clone + serde::ser::Serialize + std::fmt::Debug
    {
        let mut t = DTable::new_table();
        for _ in 0..arr.shape()[1]{
            t.new_col();
        }
        for row in arr.rows(){ 
            t.push_row(row.to_vec());            
        }    
        t.clone()
    }

    /// Set the DTable title
    pub fn set_title(&mut self, title: String){
        self.title = title;
    }

    /// Creates a new, empty column with a title
    pub fn new_column(&mut self, title: String){
        self.columns.push(DColumn::new(title));
    }

    /// Creates a new, empty column without a title
    pub fn new_col(&mut self){
        self.columns.push(DColumn::new_column());
    }

    /// Get the titles of all the DColumns as an array
    pub fn headers(&mut self) -> Vec<String> {
        let mut out = Vec::new();
        for col in &self.columns {
            out.push(col.title.clone());
        }
        out
    }

    /// Gets the selected row at index `row` as an Array of Json Values 
    /// or an array of Null elements
    pub fn get_row(&mut self, row: usize) -> Vec<Value> {
        let mut res: Vec<Value> = Vec::new();
        for col in &self.columns {
            match col.cells.get(row) {
                Some(val) => res.push(val.clone().to_owned()),
                None => res.push(Value::Null),
            }
        }
        res
    }

    /// Gets the selected row at index `row` as an Array of Json Values 
    /// or None if one of the elements in column is missing
    pub fn get_row_opt(&mut self, row: usize) -> Option<Vec<Value>> {
        let mut res: Vec<Value> = Vec::new();
        for col in &self.columns {
            match col.cells.get(row) {
                Some(val) => res.push(val.clone().to_owned()),
                None => return None,
            }
        }
        Some(res)
    }
     
    /// Gets the selected row at index `row` as an Array of references to Json Values 
    /// or an array of references to Null elements
    pub fn get_row_mut(&mut self, row: usize) -> Vec<&mut Value> {
        let mut res: Vec<&mut Value> = Vec::new();
        for col in self.columns.iter_mut() {
            match col.cells.get_mut(row) {
                Some(val) => res.push(val),
                None => break, // @TODO Make unit tests for edge cases
            }
        }
        res
    }
    

    /// Push a whole new DColumn to the table
    pub fn push_column(&mut self, col: &DColumn){
        self.columns.push(col.clone());
    }

    /// Push a whole new row to the table
    /// Row lenght must match, otherwise it will be trimmed or filled with empty strings.
    pub fn push_row<T>(&mut self, row: Vec<T>) 
    where
        T: serde::Serialize
    {
        for (i, col) in self.columns.iter_mut().enumerate() {
            
            match row.get(i){
                Some(val) => col.push(val),
                None => col.push(""),
            }
        }
    }

}

/// IntoIterator for rows in DTable
impl IntoIterator for DTable {
    type Item = Vec<Value>;
    type IntoIter = DTableRowIntoIterator;

    fn into_iter(self) -> Self::IntoIter {
        DTableRowIntoIterator {
            table: self,
            index: 0,
        }
    }
}

/// Iterator for rows in DTable
impl Iterator for DTableRowIntoIterator {
    type Item = Vec<Value>;
    fn next(&mut self) -> Option<Vec<Value>> {
        let result: Option<Vec<Value>> = self.table.get_row_opt(self.index);
        self.index += 1;
        result
    }
}

/// DTableRowIntoIterator to iterate over DTable rows
pub struct DTableRowIntoIterator {
    table: DTable,
    index: usize,
}
impl fmt::Display for DTable {
    #[doc(hidden)]
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut formatted_string = String::new();
        if self.title != "" { formatted_string = format!("{}:\n", self.title); }
        let mut at_least_a_title = false;
        for column in &self.columns {
            if column.title != "" {
                formatted_string = format!("{}{}\t", formatted_string, column.title);
                at_least_a_title = true;
            }
        }
        if at_least_a_title {
            formatted_string = format!("{}\n{:->width$}\n", formatted_string, "", width=(self.columns.len() * 9));
        }
        let data = self.clone();
        for row in data {
            for cell in row {
                match cell {
                    Value::Null => { formatted_string = format!("{}Null\t", formatted_string)},
                    Value::Number(n) => {
                        formatted_string = format!("{}{}\t", formatted_string, n.as_f64().unwrap())
                    },
                    Value::String(s) => {
                        formatted_string = format!("{}{}\t", formatted_string, s.as_str().chars().take(7).collect::<String>())
                    },
                    Value::Bool(b) => {
                        formatted_string = format!("{}{}\t", formatted_string, b)
                    },
                    _ => { formatted_string = format!("{}\t", formatted_string) },
                };
            }
            formatted_string = format!("{}\n", formatted_string);
        }
        write!(f, "{}", formatted_string)
    }
}