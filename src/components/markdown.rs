/* Roadmap:
- [ ] Create branch for component
- [x] Make component
    - [x] modify mod.rs
    - [x] modify lib.rs
    - [x] modify list in lib.rs docs
    - [x] modify list in Readme
- [x] Document component
- [x] Example for component
    - [x] successfully run example
- [x] Finally, commit a finished element
- [ ] Improve the element, following the TODOs
*/
//! ## Description
//! 
//! The `<Markdown>` component is a Yew component that parses and displays strings in Markdown.
//! It supports [CommonMark](https://commonmark.org/) Markdown, but also extensions.
//! 
//! Under the hood it uses the [pulldown-cmark](https://docs.rs/pulldown-cmark) crate.
//! 
//! ## Usage
//! 
//! The `<RawHtml>` component must be used in a Yew `html!` macro.
//! 
//! ```
//! html! {
//!     <Markdown data={"_foo_".to_string()} />
//! };
//! ```
//! 
//! ## Properties
//! 
//! The component can accept the following poperties:
//! 
//! - `data`: `String` Prop to pass a Markdown String to <Markdown> component
//! - `id:` `Option<String>` Element's ID (optional)
//! - `class`: `Option<String>` Element's CSS class; when specified, it replaces the default `rs-markdown`
//! - `tables`: `Option<bool>` Table Markdown extension; Enabled (true) by default
//! - `footnotes`: `Option<bool>` Footnotes Markdown extension; Enabled (true) by default
//! - `strikethrough`: `Option<bool>` Strikethrough Markdown extension; Enabled (true) by default
//! - `tasklists`: `Option<bool>` Tasklists Markdown extension; Enabled (true) by default
//! - `punctuation`: `Option<bool>` Smart punctuation Markdown extension; Enabled (true) by default
//! - `heading`: `Option<bool>` Heading attributes (id and classes) Markdown extension; Enabled (true) by default 
//! 
//! ## CSS Class
//! This element is wrapped in a `<div>` styled with the class `rs-markdown`
//! The class can be changed with the `class` property.
//! 
//! ## ID
//! This element is wrapped in a `<div>` whose `id` can be set with the `id` property.
// @TODO add explanation on extensions; check https://www.markdownguide.org/extended-syntax/
// @TODO explore whether to use children for the component instead
use yew::{function_component, html, Html, Properties};
use crate::prelude::*;

/// Markdown component props
#[derive(Properties, PartialEq)]
pub struct MarkdownProps{
    /// Prop to pass a Markdown String to `<Markdown>` component
    pub data: String,
    /// Element's ID (optional)
    #[prop_or_default]
    pub id: String,
    /// Element's CSS class, to replace the default `rs-markdown`
    #[prop_or("rs-markdown".to_string())]
    pub class: String,

    /// Table Markdown extension; Enabled (true) by default.
    pub tables: Option<bool>, 
    /// Footnotes Markdown extension; Enabled (true) by default.
    pub footnotes: Option<bool>,
    /// Strikethrough Markdown extension; Enabled (true) by default. 
    pub strikethrough: Option<bool>, 
    /// Tasklists Markdown extension; Enabled (true) by default.
    pub tasklists: Option<bool>, 
    /// Smart punctuation Markdown extension; Enabled (true) by default.
    pub punctuation: Option<bool>, 
    /// Heading attributes (id and classes) Markdown extension; Enabled (true) by default.
    pub heading: Option<bool>, 
}

/// Markdown component
#[function_component(Markdown)]
pub fn markdown(props: &MarkdownProps) -> Html {    
    // pulldown_cmark init
    use pulldown_cmark::Options;    
    let mut options = Options::all(); // enables all the extensions
    // Disable extensions one by one as per requests
    if props.tables == Some(false) { options.toggle(Options::ENABLE_TABLES); }
    if props.footnotes == Some(false) { options.toggle(Options::ENABLE_FOOTNOTES); }
    if props.strikethrough == Some(false) { options.toggle(Options::ENABLE_STRIKETHROUGH); }
    if props.tasklists == Some(false) { options.toggle(Options::ENABLE_TASKLISTS); }
    if props.punctuation == Some(false) { options.toggle(Options::ENABLE_SMART_PUNCTUATION); }
    if props.heading == Some(false) { options.toggle(Options::ENABLE_HEADING_ATTRIBUTES); }
    
    //Parsing
    let parser = pulldown_cmark::Parser::new_ext(&props.data, options);

    // Output to String
    let mut html_output = String::new();
    pulldown_cmark::html::push_html(&mut html_output, parser);
    html! {
        <RawHtml data={ html_output } class={ props.class.clone() } id={ props.id.clone() } />
    }
}