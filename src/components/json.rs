/* Roadmap:
- [x] Create branch for component
- [x] Make component
    - [x] modify mod.rs
    - [x] modify lib.rs
    - [x] modify list in lib.rs docs
    - [x] modify list in Readme
- [x] Document component
- [x] Example for component
    - [x] successfully run example
- [x] Finally, commit a finished element
- [ ] Improve the element, following the TODOs
*/
//! ## Description
//! 
//! The `<Json>` component is a Yew component that creates a context for Json data.
//! 
//! ## Properties
//! 
//! The component can accept the following poperties:
//! 
//! - `data`: `String` Prop to pass a JSON String to <Json> component
//! - `id:` `Option<String>` Context's ID (optional)
//! - `class`: `Option<String>` Context's CSS class; when specified, it replaces the default `rs-json`
//! 
//! ## CSS Class
//! The whole context is wrapped in a `<div>` styled with the class `rs-json`
//! The class can be changed with the `class` property.
//! 
//! ## ID
//! The whole context is wrapped in a `<div>` whose `id` can be set with the `id` property.
use std::fmt;
use yew::{ContextProvider, Children, function_component, html, Html, Properties, use_state};
use serde_json::Value;

/// Json component props
#[derive(Properties, PartialEq)]
pub struct JsonProps {
    #[doc(hidden)]
    #[prop_or_default]
    pub children: Children,
    /// Prop to pass a String Json representation to `<Json>` component
    pub data: String,
    /// Element's ID (optional)
    #[prop_or_default]
    pub id: String,
    /// Element's CSS class, to replace the default `rs-json`
    #[prop_or("rs-json".to_string())]
    pub class: String,
}

/// Json context
/// 
/// Can be used by any of the children of a `<Json>` component wth `use_context`
/// 
/// ## Usage
/// 
/// Consider the following example:
/// 
/// ```
/// use rustream::prelude::*;
/// 
/// fn main() {
///     yew::start_app::<App>();
/// }
/// 
/// #[function_component(App)]
/// pub fn app() -> Html {
///     let json_data = r#"
///         {
///             "name": "John Doe",
///             "age": 42,
///         }"#;
///     html! {
///         <Json data={ json_data }>
///             <ViewJson />
///         </Json>
///     }
/// }
/// 
/// #[function_component(ViewJson)]
/// pub fn view_json() -> Html {
///     let json = use_context::<JsonContext>().expect("no ctx found");
///     let code = format!("
///     ```
///     {:?}
///     ```
///     ", json.data);
///     html! {
///         <Markdown data={ code } />
///     }
/// }
/// ```
/// 
/// We can create a custom component, `ViewJson` that uses the `<Markdown>` component 
/// to render the JSON data. Inside `ViewJson` we can use `use_context` 
/// to access the data from the `JsonContext`. 
/// 
/// This is exactly how our standard `<ViewJson>` is coded, but wrapped in a `<pre>`.
/// See the [`dataview`] docs for more details.
/// 
// @TODO: create an option to make the JSON available to the whole app, saved in the page data
#[derive(Clone, Debug, PartialEq)]
pub struct JsonContext {
    /// This Option contains the serialized JSON (a `serde_json::Value`) or `None` if error occurred.
    pub data: Option<Value>,
}
impl fmt::Display for JsonContext {
    #[doc(hidden)]
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match &self.data {
            Some(data) => write!(f, "{}", serde_json::to_string_pretty(data).unwrap()),
            None => write!(f, ""),
        }
    }
}


/// Json component
#[function_component(Json)]
pub fn json(props: &JsonProps) -> Html {
    let data = serde_json::from_str(props.data.as_str()).ok();
    let ctx = use_state(|| JsonContext {
        data,
    });
    html! {
        <div class={ props.class.clone() } id={ props.id.clone() }>
            <ContextProvider<JsonContext> context={(*ctx).clone()}>
                { for props.children.iter() }
            </ContextProvider<JsonContext>>
        </div>
    }
}