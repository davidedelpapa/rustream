// @TODO commented out some part of milligram that reference to a background image made with SVG
//       Try to solve the problem!
/* Roadmap:
- [x] Create branch for component
- [x] Make component
    - [x] modify mod.rs
    - [x] modify lib.rs
    - [x] modify list in lib.rs docs
    - [x] modify list in Readme
- [x] Document component
- [x] Example for component
    - [x] successfully run example
- [] Finally, commit a finished element
- [ ] Improve the element, following the TODOs
*/
//! ## Description
//! 
//! The `<LoadStyles>` component is a Yew component that loads the default CSS styles.
//! 
//! Currently, it loads [`milligram.css`], 
//! wih some coarse modifications to adapt to css-in-rust philosophy and [`stylist`].
//! 
//! Components will not work properly without the `style` (or `default`) feature.
//! However, CSS stylesheets could be loaded with a `<link>` tag in the *index.html* 
//! (see below for more details).
//! 
//! ## Usage
//! 
//! The `<LoadStyles>` component must be used in a Yew `html!` macro. 
//! In order to use it, the component must also implement `#[styled_component()]`
//!  
//! 
//! ```
//! #[styled_component(App)]
//! pub fn app() -> Html {
//!     html! {
//!         <LoadStyles />
//!     }
//! };
//! ```
//! 
//! In order to use styled components without the `style` or `default` feature, 
//! the CSS files must be linked directly in the *index.html* file with a `<link>` tag.
//! 
//! For example:
//! 
//! ```html
//! <link href="milligram.min.css" rel="stylesheet">
//! <link href="milligram.card.min.css" rel="stylesheet">
//! <link href="milligram.nav.min.css" rel="stylesheet">
//! ```
//! 
//! The CSS files to be linked are found in the `vendor/min/` folder of the project. 
//! In a near future the stylesheet will be unified and made available through a CDN.
//! [`milligram.css`]: https://milligram.io/
//! [`stylist`]: https://crates.io/crates/stylist
use stylist::StyleSource;
use stylist::yew::{styled_component, Global};
use yew::{html, Html};

/// LoadStyles component
#[styled_component(LoadStyles)]
pub fn load_styles() -> Html {
    let css_file = include_str!("../../vendor/milligram.css");
    let nav_css_file = include_str!("../../vendor/milligram.nav.css");
    let card_css_file = include_str!("../../vendor/milligram.card.css");

    let css_style: StyleSource = css_file.parse().expect("Error in vendor/milligram.css");
    let nav_style: StyleSource = nav_css_file.parse().expect("Error in vendor/milligram.nav.css");
    let card_style: StyleSource = card_css_file.parse().expect("Error in vendor/milligram.card.css");

    html! {
        <>  
            <Global css={css_style} />
            <Global css={nav_style} />
            <Global css={card_style} /> 
        </>
    }
}