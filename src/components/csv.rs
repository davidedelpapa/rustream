/* Roadmap:

- [x] Create branch for component
- [x] Make component
    - [x] modify mod.rs
    - [x] modify lib.rs
    - [ ] modify list in lib.rs docs
    - [ ] modify list in Readme
- [x] Document component
- [x] Example for component
    - [x] successfully run example
- [ ] Finally, commit a finished element
- [ ] Improve the element, following the TODOs
 */
//! ## Description
//! 
//! The `<Csv>` component is a Yew component that creates a context for CSV data.
//! 
//! ## Properties
//! 
//! The component can accept the following poperties:
//! 
//! - `data`: `String` Prop to pass a CSV String to <Csv> component
//! - `id:` `Option<String>` Context's ID (optional)
//! - `class`: `Option<String>` Context's CSS class; when specified, it replaces the default `rs-json`
//! - `delimiter`: Option<u8> Custom delimiter
//! - `headers`: Option<bool> Wether the first line of the CSV string is the headers or not. Default `true`
//! - `flexible`: Option<bool> Wether to be flexible with missing columns or not. Default `true`
//! - `trim`: Option<bool> Wether to trim data or not
//! 
//! ## CSS Class
//! The whole context is wrapped in a `<div>` styled with the class `rs-csv`
//! The class can be changed with the `class` property.
//! 
//! ## ID
//! The whole context is wrapped in a `<div>` whose `id` can be set with the `id` property.
//! 
//! ## Delimiter
//! The `delimiter` property accepts a `u8`, therefore only an original ASCII character.
//! Default is `b','` (comma), but it could be `b'/t'` (tab), `b';'` (semi-colon), `b':'` (colon), or anything else.
//! 
//! ## Headers
//! Wether the first line of the CSV string is to be treated as the headers or not. 
//! 
//! In case the dataset does not contain a header row, data can still be handled; 
//! however, it should be better to create one and push it as the first row of the CSV dataset.
//! 
//! ## Flexible
//! Wether to be flexible with missing columns in each field or not. 
//! 
//! When data is missing, the user must be careful on how eahc field (row) is parsed.
//! Allowing for a more strict data ingestion with `flexible={false}` 
//! could catch some mistakes at the root, with the downside of rejecting a falulty dataset as a whole, 
//! with no sanitation allowed.
//! 
//! ## Trim
//! Ordinarily, data must not contain space before or after the delimiter character. 
//! Allowing the trimming of whitespace results in a more flexible data ingestion.
//! 
//! When `true`, only ASCCi whitespace correspondig to the set `[\t\n\v\f\r ]` is trimmed. 
//! Both headers and fileds are trimmed.
//! 
use ::csv::{Trim, ReaderBuilder, StringRecord};
use yew::{ContextProvider, Children, function_component, html, Html, Properties, use_state};
use crate::prelude::*;

/// Csv component props
#[derive(Properties, PartialEq)]
pub struct CsvProps {
    #[doc(hidden)]
    #[prop_or_default]
    pub children: Children,
    /// Prop to pass a String CSV representation to `<Csv>` component
    pub data: String,
    /// Element's ID (optional)
    #[prop_or_default]
    pub id: String,
    /// Element's CSS class, to replace the default `rs-csv`
    #[prop_or("rs-csv".to_string())]
    pub class: String,

    /// Custom delimiter. 
    /// 
    /// Default is `b','` but it could be `b'/t'`, `b';'`, `b':'`,or anything else. 
    /// It accepts a `u8`, therefore only an original ASCII character.
    pub delimiter: Option<u8>,
    /// Wether the first line of the CSV string is the headers or not. Default `true`.
    pub headers: Option<bool>,
    /// Wether to be flexible with missing columns in each field or not. Default `true`.
    pub flexible: Option<bool>,
    /// Wether to trim data or not. 
    /// 
    /// Ordinarily, data must not contain space before or after the delimiter character. 
    /// Allowing the trimming of whitespace results in a more flexible data ingestion.
    /// 
    /// When `true`, only ASCCi whitespace correspondig to the set `[\t\n\v\f\r ]` is trimmed. 
    /// Both headers and fileds are trimmed.
    /// 
    /// Default `true`.
    pub trim: Option<bool>,
}

/// Csv context
/// 
/// Can be used by any of the children of a `<Csv>` component wth `use_context`
/// 
/// ## Usage
/// 
// @TODO: create an option to make the JSON available to the whole app, saved in the page data
#[derive(Debug, Clone, PartialEq)]
pub struct CsvContext {
    /// This context contains the serialized CSV as a `DTable`
    pub data: DTable,
}

/// Csv component
// @TODO implement all the options in https://docs.rs/csv/latest/csv/struct.ReaderBuilder.html
#[function_component(Csv)]
pub fn csv(props: &CsvProps) -> Html {
    // Create the reader and apply the selected options
    let mut rdr = ReaderBuilder::new();
    if props.delimiter != None {
        let delim = props.delimiter.unwrap();
        rdr.delimiter(delim);
    }
    if props.headers != None {
        let headers = props.headers.unwrap();
        rdr.has_headers(headers);
    }
    if props.flexible != None {
        let flexible = props.flexible.unwrap();
        rdr.flexible(flexible);
    }
    if props.trim != Some(false) {
        rdr.trim(Trim::All);
    }
    let mut data = rdr.from_reader(props.data.as_bytes());
    let mut table: DTable = DTable::new_table();
    // Set headers and new columns
    if data.has_headers() {
        let headers = data.headers().unwrap();
        for rec in headers {
            table.new_column(rec.to_string());
        }
    } else {
        let mut record = StringRecord::new();
        match data.read_record(&mut record){
            Ok(is_record) => if is_record {
                for _ in &record {
                    table.new_col();
                }
            },
            Err(_) => (),
        }
        
    }
    // Fill the table
    // @TODO handle missing data in columns
    for record in data.records() {
        match record {
            Ok(rec) => {
                //let row: Vec<
                table.push_row(rec.iter().map(|e| e.to_string()).collect())},
            Err(_) => {},
        }        
    }
    
    let ctx = use_state(|| CsvContext {
        data: table,
    });
    html! {
        <div class={ props.class.clone() } id={ props.id.clone() }>
            <ContextProvider<CsvContext> context={(*ctx).clone()}>
                { for props.children.iter() }
            </ContextProvider<CsvContext>>
        </div>
    }
}