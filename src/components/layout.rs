/* Roadmap:
- [x] Create branch for component
- [x] Make component
    - [x] modify mod.rs
    - [x] modify lib.rs
    - [x] modify list in lib.rs docs
    - [x] modify list in Readme
- [x] Document component
- [x] Example for component
    - [x] successfully run example
- [x] Finally, commit a finished element
- [ ] Improve the element, following the TODOs
*/
//! # Description
//! 
//! This contains various components useful to create a layout for a web page.
//! 
//! ## NavBar
//! 
//! The `<NavBar>` element creates a `<header>` element with a collapsible navigation bar inside.
//! 
//! ### Usage
//! 
//! The `<NavBar>` component must be used in a Yew `html!` macro.
//! 
//! In order to use it properly, the `<LoadStyles>` component must be declared at the beginning of the page.
//! 
//! The `<NavBar>` component accepts two kinds of children:
//! - a `<Logo>` component
//! - a `<NavMenu>` component
//! 
//! ```
//! html! {
//!     <NavBar sticky={true}>
//!         <Logo>{"myLogo"}</Logo>
//!         <NavMenu>
//!             <li><a href={"#"} >{ "Home" }</a></li>
//!             <li><a href={"#"} >{ "Shop" }</a></li>
//!             <li><a href={"#"} >{ "About" }</a></li>
//!         </NavMenu>
//!     </NavBar>
//! };
//! ```
//! 
//! ### Properties
//! 
//! The component can accept the following poperties:
//! 
//! - `id`: `Option<String>` Element's ID (optional)
//! - `sticky`: `Option<bool>` Whether the NavBar is sticky. Default is `false`.
//! 
//! ### ID
//! This element is wrapped in a `<div>` whose `id` can be set with the `id` property.
//! 
//! ## Logo
//! 
//! The `<Logo>` element creates a logo inside a `<NavBar>` (see component above).
//! 
//! ### Usage
//! 
//! The `<Logo>` component must be used in a Yew `html!` macro, inside a inside a `<NavBar>`.
//! 
//! You can safely inline a `<img>` and a string of text inside this element.
//! 
//! ```
//! html! {
//!     <Logo><img src={"https://via.placeholder.com/35"} />{"myLogo"}</Logo>
//! };
//! ```
//! 
//! ### Properties
//! 
//! The component can accept the following poperties:
//! 
//! - `id`: `Option<String>` Element's ID (optional)
//! 
//! ### ID
//! This element is wrapped in a `<span>` whose `id` can be set with the `id` property.
//! 
//! ## NavMenu
//! 
//! The `<NavMenu>` element creates the entries for the navigation menu inside a `<NavBar>` (see component above).
//! 
//! ### Usage
//! 
//! The `<NavMenu>` component must be used in a Yew `html!` macro, inside a inside a `<NavBar>`.
//! 
//! The `<NavMenu>` component must contain `<li>` wrapped elements. 
//! It is a wrapper for a `<ul>` of sorts.
//! 
//! ```
//! html! {
//!     <NavMenu>
//!         <li><a href={"#"} >{ "Home" }</a></li>
//!         <li><a href={"#"} >{ "Shop" }</a></li>
//!         <li><a href={"#"} >{ "About" }</a></li>
//!     </NavMenu>
//! };
//! ```
//! 
//! ### Properties
//! 
//! The component can accept the following poperties:
//! 
//! - `id`: `Option<String>` Element's ID (optional)
//! 
//! ### ID
//! The `<li>` elements are wrapped in a `<ul>` whose `id` can be set with the `id` property.
//! 
//! ## Card
//! 
//! The `<Card>` element creates a Card to group information in a more visible way.
//! 
//! ### Usage
//! 
//! The `<Card>` component must be used in a Yew `html!` macro.
//! 
//! In order to use it properly, the `<LoadStyles>` component must be declared at the beginning of the page.
//! 
//! The `<Card>` component accepts two kinds of children:
//! - a `<CardHeader>` component
//! - a `<CardBody>` component
//! 
//! ```
//! html! {
//!     <Card class = {"color1"}>
//!         <CardHeader class= {"color1"}>{ "Card header" }</CardHeader>
//!         <CardBody class= {"color1"}>
//!             <p>{ "Card Description" }</p>
//!         </CardBody>
//!     </Card>
//! };
//! ```
//! 
//! ### Properties
//! 
//! The component can accept the following poperties:
//! 
//! - `id`: `Option<String>` Element's ID (optional)
//! - `class`: `Option<String>` Element's CSS class (optional)
//! - `shadow`: `Option<bool>` If `true` it adds a shadow effect on the card.
//! 
//! ### ID
//! This element is wrapped in a `<div>` whose `id` can be set with the `id` property.
//! 
//! ### CSS
//! This element accepts CSS classes. 
//! 
//! Four color classes are already created:
//! - `color1`: `border-color`: `#ffafcc` and `color`: `#ff006e`
//! - `color2`: `border-color`: `#a2d2ff` and `color`: `#3a86ff`
//! - `color3`: `border-color`: `#fcbf49` and `color`: `#f77f00`
//! - `color4`: `border-color`: `#606c38` and `color`: `#283618`
//! 
//! #### Custom colors
//! In order to adapt your own color-scheme, 
//! the following three classes must be declared for each color:
//! 
//! `.card`:
//! 
//! ```css
//! .card.own_color {
//!     border-color: #<color-code>;
//! }
//! ```
//! 
//! `.card-header`:
//! 
//! ```css
//! .card-header.own_color {
//!     color: #<color-code>;
//!     border-color: #<color-code>;
//! }
//! ```
//! 
//! `.card-body`:
//! 
//! ```css
//! .card-body.own_color {
//!     color: #<color-code>;
//!     border-color: #<color-code>;
//! }
//! ```
//! 
//! ## CardHeader
//! 
//! The `<CardHeader>` element creates a header inside a `<Card>` element.
//! 
//! ### Usage
//! 
//! The `<CardHeader>` component must be used inside a `<Card>` element, in a Yew `html!` macro.
//! 
//! ```
//! html! {
//!     <Card class = {"color2"}>
//!         <CardHeader class= {"color2"}>{ "Card header" }</CardHeader>
//!     </Card>
//! };
//! ```
//! ### Properties
//! 
//! The component can accept the following poperties:
//! 
//! - `id`: `Option<String>` Element's ID (optional)
//! - `class`: `Option<String>` Element's CSS class (optional)
//! 
//! ### ID
//! This element is wrapped in a `<div>` whose `id` can be set with the `id` property.
//! 
//! ### CSS
//! This element accepts CSS classes. 
//! 
//! Four color classes are already created:
//! - `color1`: `border-color`: `#ffafcc` and `color`: `#ff006e`
//! - `color2`: `border-color`: `#a2d2ff` and `color`: `#3a86ff`
//! - `color3`: `border-color`: `#fcbf49` and `color`: `#f77f00`
//! - `color4`: `border-color`: `#606c38` and `color`: `#283618`
//! 
//! See the `<Card>` elements for notes about customizing the color-scheme.
//! 
//! ## CardBody
//! 
//! The `<CardBody>` element creates a header inside a `<Card>` element.
//! 
//! ### Usage
//! 
//! The `<CardBody>` component must be used inside a `<Card>` element, in a Yew `html!` macro.
//! 
//! It is best suited to hold `<p>` and `<i>` elements, or just plain text.
//! 
//! ```
//! html! {
//!     <Card class = {"color3"}>
//!         <CardBody class= {"color3"}>
//!             <p>{ "Card Description" }</p>
//!         </CardBody>
//!     </Card>
//! };
//! ```
//! 
//! ### Properties
//! 
//! The component can accept the following poperties:
//! 
//! - `id`: `Option<String>` Element's ID (optional)
//! - `class`: `Option<String>` Element's CSS class (optional)
//! 
//! ### ID
//! This element is wrapped in a `<div>` whose `id` can be set with the `id` property.
//! 
//! ### CSS
//! This element accepts CSS classes. 
//! 
//! Four color classes are already created:
//! - `color1`: `border-color`: `#ffafcc` and `color`: `#ff006e`
//! - `color2`: `border-color`: `#a2d2ff` and `color`: `#3a86ff`
//! - `color3`: `border-color`: `#fcbf49` and `color`: `#f77f00`
//! - `color4`: `border-color`: `#606c38` and `color`: `#283618`
//! 
//! See the `<Card>` elements for notes about customizing the color-scheme.
//! 
//! ## Section
//! 
//! The `<Section>` element creates a separate section in the document.
//! 
//! ### Usage
//! 
//! The `<Section>` component must be used in a Yew `html!` macro.
//! 
//! ```
//! html! {
//!     <Section name={"mySection"}>
//!         <p>{ "This is a new section" }</p>
//!     </Section>
//! };
//! ```
//! 
//! ### Properties
//! 
//! The component can accept the following poperties:
//! 
//! - `name`: `String` Element's name (it sets also the id)
//! - `class`: `Option<String>` Element's CSS class (optional)
//! - `shadow`: `Option<bool>` When false does not paint a shadow aroun the section
//! 
//! ### ID
//! This element is wrapped in a `<div>` whose `id` is always set by the `name` property.
//! 
//! ### CSS
//! This element accepts CSS classes. 
//! 
//! ## Page
//! 
//! The `<Page>` element creates a virtul page in the document.
//! 
//! It is useful to place typesetting elements inside, 
//! and a `<Toc>` element to create a table of content
//! 
//! ### Usage
//! 
//! The `<Page>` component must be used in a Yew `html!` macro.
//! 
//! ```
//! html! {
//!     <Page>
//!         <Toc />
//!         <Header data={ "This is a title" } />
//!     </Page>
//! };
//! ```
//! 
//! ### Properties
//! 
//! The component does not accept poperties so far. it is a pure virtual container. 
//! 
//! ## Toc
//! 
//! The `<Toc>` element creates a table of content for each `<header>` element found,
//! when both are placed in a `<Page>` pair.
//! 
//! > *Note*: when the `<Header>` elements have an `id` set, the `<Toc>` also creates a link to the header.
//! 
//! ### Usage
//! 
//! The `<Toc>` component must be used in a Yew `html!` macro.
//! 
//! ```
//! html! {
//!     <Page>
//!         <Toc />
//!         <Header data={ "This is a title" } />
//!     </Page>
//! };
//! ```
//! 
//! ### Properties
//! 
//! The component can accept the following poperties:
//! 
//! - `id`: `String` The element's id (optional)
//! - `class`: `Option<String>` Element's CSS class (optional)
//! - `fallback`: `Option<Html>` `Html` elements to visulize in case it would take time for the component to load. Default is `html! {<div>{"Loading..."}</div>}`
//! 
//! ### ID
//! This element is wrapped in a `<div>` whose `id` can be set by the `id` property.
//! 
//! ### CSS
//! This element accepts CSS classes; default is `rs-toc`. 
//! 
// TODO: Finish to document this module
// TODO: add possibility to enumerate chapters
use std::rc::Rc;
use yew::{function_component, html, Html, Children, ContextProvider, Suspense, Properties, 
    use_context, Reducible, UseReducerHandle, use_reducer};

/// TOCEntry struct
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct TOCEntry {
    /// Id of the element for linking
    pub id: String,
    /// Title of the element
    pub title: String,
    /// Level of indentation (h1, h2, h3, ...)
    pub level: u8,
}
impl TOCEntry{
    /// Create a new TOCEntry
    pub fn new(id: String, title: String, level: u8,) -> TOCEntry {
        TOCEntry{
            id,
            title,
            level,
        }
    }
}


/// PageState contains the state for the Page and its children
#[derive(Debug, Clone, PartialEq)]
pub struct PageState {
    /// The Table of Content
    pub toc: Vec<TOCEntry>,
    /// The pages' title
    pub title: String,
}
impl Default for PageState {
    fn default() -> Self {
        Self { 
            toc: Vec::new(),
            title: String::new(),
        }
    }
}

/// Page action (to set PageContext)
#[derive(Debug, Clone, Eq, PartialEq)]
pub enum PageAction {
    /// Action to set the Page's TOC
    SetToc(TOCEntry),
    /// Action to set the Page's title
    SetPageTitle(String),
}

impl Reducible for PageState {
    type Action = PageAction;

    fn reduce(self: Rc<Self>, action: Self::Action) -> Rc<Self> {
        match action {
            PageAction::SetToc(tocentry) => {
                // ATTENTION: insert in reverse order, as it seems yew calls them in reverse
                let mut toc = self.toc.clone();
                toc.insert(0, tocentry);
                PageState { 
                    toc,
                    title: self.title.clone(),
                }.into()
            },
            PageAction::SetPageTitle(title) => PageState { 
                toc: self.toc.clone(),
                title,
            }.into(),
        }
    }
}

/// PageContext context
#[derive(Clone, Debug, PartialEq)]
pub struct PageContext{
    #[doc(hidden)]
    pub inner: UseReducerHandle<PageState>,
}

/// Page component props
#[derive(Properties, PartialEq)]
pub struct PageProps {
    #[doc(hidden)]
    #[prop_or_default]
    pub children: Children,
}

/// Page component
#[function_component(Page)]
pub fn page(props: &PageProps) -> Html {
    let page_state = use_reducer(PageState::default);
    let page_context = PageContext {
        inner: page_state,
    };
    
    
    html! {
        <>
            <ContextProvider<PageContext> context={page_context}>
                { for props.children.iter() }
            </ContextProvider<PageContext>>            
        </>
    }
}

/// Toc component props
#[derive(Properties, PartialEq)]
pub struct TocProps{
    /// Element's ID (optional)
    #[prop_or_default]
    pub id: String,
    /// Element's CSS class, to replace the default `rs-toc`
    #[prop_or("rs-toc".to_string())]
    pub class: String,
    /// Fallback Html for when the TOC elements are generated.
    /// It defaults to `html! {<div>{"Loading..."}</div>}`
    #[prop_or(html! {<div>{"Loading..."}</div>})]
    pub fallback: Html,
}

/// Toc component
#[function_component(Toc)]
pub fn toc(props: &TocProps) -> Html {
    let ctx = use_context::<PageContext>();
    let fallback = props.fallback.clone();

    let render_list = |ctx: PageContext| html!{
        <Suspense {fallback}>
            <ul>
            { 
                
                for ctx.inner.toc.iter().enumerate().map(|(idx, e)| {
                    if e.id != "".to_string() {
                        let id = e.id.clone();
                        html!{<li key={idx}><a href={format!("#{id}")}>{e.title.clone()}</a></li>}
                    }
                    else {
                        html!{<li key={idx}>{e.title.clone()}</li>}
                    }
                }) 
            }
            </ul>
        </Suspense> 
    };
    match ctx {
        Some(ctx) => {            
            render_list(ctx.to_owned())
        },
        None => html!{},

    }
}

/// NavBar component props
#[derive(Properties, PartialEq)]
pub struct NavBarProps{
    #[doc(hidden)]
    #[prop_or_default]
    pub children: Children,
    /// Element's ID (optional)
    #[prop_or_default]
    pub id: String,

    /// Whether the NavBar is sticky. Default is `false`.
    pub sticky: Option<bool>,
}

/// NavBar component
#[function_component(NavBar)]
pub fn navbar(props: &NavBarProps) -> Html {
    let mut sticky_class = String::new();
    if props.sticky == Some(true) {
        sticky_class = "fixed".to_owned();
    }
    html! {
        <div id={ props.id.clone() }>
            <header class = {sticky_class} >
                { for props.children.iter() }
            </header>
        </div>
    }
}

/// Logo component props
#[derive(Properties, PartialEq)]
pub struct LogoProps{
    #[doc(hidden)]
    #[prop_or_default]
    pub children: Children,
    /// Element's ID (optional)
    #[prop_or_default]
    pub id: String,
}

/// Logo component
#[function_component(Logo)]
pub fn logo(props: &LogoProps) -> Html {
    html! {
        <span class={"nav-brand"} id={ props.id.clone() } >
        { for props.children.iter() }
        </span>
    }
}

/// NavMenu component props
#[derive(Properties, PartialEq)]
pub struct NavMenuProps{
    #[doc(hidden)]
    #[prop_or_default]
    pub children: Children,
    /// Element's ID (optional)
    #[prop_or_default]
    pub id: String,
}

/// NavMenu component
#[function_component(NavMenu)]
pub fn navmenu(props: &NavMenuProps) -> Html {
    html! {
        <>
            <input type = { "checkbox"} class = {"menu-toggle"} id = {"menu-toggle"} />
            <nav class = {"header"} >
                <ul id={ props.id.clone() }>
                 { for props.children.iter() }
                </ul>
            </nav>
            <label for = {"menu-toggle"} class = {"menu-nav-toggle"} >
                <span>  </span>
            </label>
        </>
    }
}

/// Card component props
#[derive(Properties, PartialEq)]
pub struct CardProps{
    #[doc(hidden)]
    #[prop_or_default]
    pub children: Children,
    /// Element's ID (optional)
    #[prop_or_default]
    pub id: String,
    /// Element's CSS class; it can be used to style color for example (see)
    #[prop_or_default]
    pub class: String,
    /// If `true` adds a shadow to the Card
    pub shadow: Option<bool>,
}

/// Card component
#[function_component(Card)]
pub fn card(props: &CardProps) -> Html {
    let mut shadow = String::new();
    if props.shadow == Some(true) { 
        shadow = "card-shadow".to_string();
    }
    let classes = format!("card {} {}", shadow, props.class.clone()).trim_end().to_string();
    html! {
        <div id={ props.id.clone() } class = { classes }>
            { for props.children.iter() }
        </div>
    }
}

/// CardHeader component props
#[derive(Properties, PartialEq)]
pub struct CardHeaderProps{
    #[doc(hidden)]
    #[prop_or_default]
    pub children: Children,
    /// Element's ID (optional)
    #[prop_or_default]
    pub id: String,
    /// Element's CSS class; it can be used to style color for example (see)
    #[prop_or_default]
    pub class: String,
}

/// CardHeader component
#[function_component(CardHeader)]
pub fn card_header(props: &CardHeaderProps) -> Html {
    html! {
        <div id={ props.id.clone() } class = { format!("card-header {}", props.class.clone()) }>
            { for props.children.iter() }
        </div>
    }
}

/// CardBody component props
#[derive(Properties, PartialEq)]
pub struct CardBodyProps{
    #[doc(hidden)]
    #[prop_or_default]
    pub children: Children,
    /// Element's ID (optional)
    #[prop_or_default]
    pub id: String,
    /// Element's CSS class
    #[prop_or_default]
    pub class: String,
}

/// CardHeader component
#[function_component(CardBody)]
pub fn card_body(props: &CardBodyProps) -> Html {
    html! {
        <div id={ props.id.clone() } class = { format!("card-body {}", props.class.clone()) }>
            { for props.children.iter() }
        </div>
    }
}

/// Section component props
#[derive(Properties, PartialEq)]
pub struct SectionProps{
    #[doc(hidden)]
    #[prop_or_default]
    pub children: Children,
    /// Section's name: it is not optional!
    pub name: String,
    /// Element's CSS class
    #[prop_or_default]
    pub class: String,
    /// Whether the sections has a shadow (default true)
    pub shadow: Option<bool>,
}

/// Section component
#[function_component(Section)]
pub fn section(props: &SectionProps) -> Html {
    if props.shadow == Some(false) {
        html! {
            <div id={ props.name.clone() } class = { format!("section-container {}", props.class.clone()) }>
                { for props.children.iter() }
            </div>
        }
    } else {
        html! {
            <div class = { "section-container" }>
                <div id={ props.name.clone() } class = { format!("section {}", props.class.clone()) }>
                    { for props.children.iter() }
                </div>
            </div>
        }
    }
}
