/// The `<DataView>` component
pub mod dataview;
/// The `<Json>` component
#[cfg(feature = "json")]
pub mod json;
/// The `<Csv>` component
#[cfg(feature = "csv")]
pub mod csv;
/// The `<Latex>` component
#[cfg(feature = "latex")]
pub mod latex;
/// The `<Markdown>` component
#[cfg(feature = "markdown")]
pub mod markdown;
/// The `<RawHtml>` component
pub mod rawhtml;
/// The *Typesetting* module, containing useful elements to style the page
pub mod typesetting;
/// The *LoadStyles* component
#[cfg(feature = "style")]
pub mod style;
/// The *Layout* module, containing useful components to create sections in a web page.
pub mod layout;
/// The `<Canvas>` component
pub mod canvas;