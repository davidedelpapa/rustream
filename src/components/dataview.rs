/* Roadmap:
- [ ] Create branch for component
- [x] Make component
    - [x] modify mod.rs
    - [x] modify lib.rs
    - [x] modify list in lib.rs docs
    - [x] modify list in Readme
- [x] Document component
- [ ] Example for component
    - [ ] successfully run example
- [x] Finally, commit a finished element
- [ ] Improve the element, following the TODOs
*/
//! ## Description
//! 
//! The `<DataView>` component is a component that displays data from any of Rustream Data Context 
//! or any Rust type that implements a default formatter.
//! 
//! For convenience there are also some implementations for specific cases:
//! - <ViewJson> 
//! 
//! ## Usage
//! 
//! The `<DataView>` component must be used in a Yew `html!` macro.
//! 
//! ```
//! html! {
//!     <DataView<String> data={"foo".to_string()} } />
//! };
//! ```
//! 
//! ## Properties
//! 
//! The component can accept the following poperties:
//! 
//! - `data`: Viewable Prop to pass a data to <DataView> component
//! - `id:` `Option<String>` Element's ID (optional)
//! - `class`: `Option<String>` Element's CSS class; when specified, it replaces the default `rs-markdown`
//! 
//! ## CSS Class
//! This element is wrapped in a `<div>` styled with the class `rs-dataview`
//! The class can be changed with the `class` property.
//! 
//! ## ID
//! This element is wrapped in a `<div>` whose `id` can be set with the `id` property.
//! 
//! ## Implementations
//! 
//! ### ViewJson
//! 
//! The `<ViewJson>` component is a wrapper for `<DataView<JsonContext> wrapper={DataViewWrapper::Pre} />`.
//! 
//! ### ViewCsv
//! 
//! The `<ViewCsv>` component shows the CSV data as a HTML table. 
//! It is not strictly a wrapper for `<DataView<CsvContext> wrapper={DataViewWrapper::Pre} />` though, 
//! as this last would show the data not as a HTML table.
//! 
// @TODO add explanation on extensions; check https://www.markdownguide.org/extended-syntax/
// @TODO explore whether to use children for the component instead
use yew::{function_component, html, Html, Properties};
use std::fmt::Display;
#[allow(unused_imports)]
use crate::prelude::*;
#[cfg(feature = "table")]
use serde_json::value::Value;

/// DataViewProps Html element prop
/// 
/// This permits to choose whether to wrap the data in a `<pre>` or a `<div>`.
/// Default: `<pre>`
#[derive(PartialEq)]
pub enum DataViewWrapper{
    /// Wraps  the `DataView` element in a `<div>`
    Div,
    /// Wraps  the `DataView` element in a `<pre>`
    Pre,
}

/// DataView component props
#[derive(Properties, PartialEq)]
pub struct DataViewProps<T>
where 
    T: PartialEq,
{
    /// Prop to pass viewable data to <DataView> component
    pub data: T,
    /// Element's ID (optional)
    #[prop_or_default]
    pub id: String,
    /// Optional element's CSS class, to replace the default `rs-dataview`
    #[prop_or("rs-dataview".to_string())]
    pub class: String,
    /// Element's wrapper. Default `DataViewWrapper::Pre` (in alternative: `DataViewWrapper::Div`)
    pub wrapper: Option<DataViewWrapper>,
}
// @TODO choice whether to use <pre> or <div>

/// DataView component
#[function_component(DataView)]
pub fn dataview<T>(props: &DataViewProps<T>) -> Html 
where
    T: PartialEq + Display,
{
    let print = format!("{}", props.data);
    match props.wrapper {
        Some(DataViewWrapper::Div) => html! {
            <div class={ props.class.clone() } id={ props.id.clone() }>
                { print }
            </div>
        },
        _ => html! {
            <pre class={ props.class.clone() } id={ props.id.clone() }>
                { print }
            </pre>
        },
    }    
}

/// ViewJson component
#[cfg(feature = "json")]
#[function_component(ViewJson)]
pub fn view_json() -> Html {
    let json = use_context::<JsonContext>().expect("no ctx found");
    html! {
        <DataView<JsonContext> data={ json } wrapper={DataViewWrapper::Pre} />
    }
}

/// ViewTable component props
#[derive(Properties, PartialEq)]
pub struct ViewTableProps{
    /// Prop to pass a Markdown String to `<Markdown>` component
    pub data: DTable,
}
/// ViewTable component
#[cfg(feature = "table")]
#[function_component(ViewTable)]
 pub fn view_table(props: &ViewTableProps) -> Html {
    show_table(props.data.clone())
}

/// ViewCsv component
#[cfg(feature = "csv")]
#[function_component(ViewCsv)]
 pub fn view_csv() -> Html {
    let csv = use_context::<CsvContext>().expect("no ctx found");
    show_table(csv.data)
 }

 #[cfg(feature = "table")]
 #[doc(hidden)]
 fn show_table(mut table: DTable) -> Html {
    let headers = table.headers();
    
    let mut header_row = "<tr><thead>".to_string();
    for field in headers {
        header_row = format!("{}<th>{}</th>", header_row, &field);
    }
    header_row = format!("{}</tr></thead>", header_row);

    let mut table_data = "<tbody>".to_string();
    for table_row in table.into_iter() {
        let mut row = "<tr>".to_string();
        for field in table_row {
            row = format!("{}<td>", row);   
            match field {
                Value::Null => { row = format!("{}Null\t", row)},
                Value::Number(n) => {
                    row = format!("{}{}\t", row, n.as_f64().unwrap())
                },
                Value::String(s) => {
                    row = format!("{}{}\t", row, s.as_str().chars().take(7).collect::<String>())
                },
                Value::Bool(b) => {
                    row = format!("{}{}\t", row, b)
                },
                _ => { row = format!("{}\t", row) },
            };
            row = format!("{}</td>", row);     
        }
        row = format!("{}</tr>", row); 
        table_data = format!("{}{}", table_data, row);
    }
    table_data = format!("{}</tbody>", table_data);
    
    let data = format!("{}{}", header_row, table_data);

    let table = gloo_utils::document().create_element("table").unwrap();
    table.set_inner_html(&data);
    Html::VRef(table.into())
 }