/* Roadmap:
- [ ] Create branch for component
- [x] Make component
    - [x] modify mod.rs
    - [x] modify lib.rs
    - [x] modify list in lib.rs docs
    - [x] modify list in Readme
- [x] Document component
- [ ] Example for component
    - [ ] successfully run example
- [x] Finally, commit a finished element
- [ ] Improve the element, following the TODOs
*/
//! ## RawHtml
//! 
//! ### Description
//! 
//! The `<RawHtml>` component is a Yew component that displays raw HTML.
//! 
//! Under the hood it uses the [`gloo_utils`] and [`VNode`].
//! 
//! ### Usage
//! 
//! The `<RawHtml>` component must be used in a Yew `html!` macro.
//! 
//! ```
//! html! {
//!     <RawHtml data={"<i>foo</i>".to_string()} />
//! };
//! ```
//! 
//! ### Properties
//! 
//! The component can accept the following poperties:
//! 
//! - `data`: `String` Prop to pass a Raw Html String to <RawHtml> component
//! - `id:` `Option<String>` Element's ID (optional)
//! - `class`: `Option<String>` Element's CSS class; when specified, it replaces the default `rs-rawhtml`
//! 
//! ### CSS Class
//! This element is wrapped in a `<div>` styled with the class `rs-rawhtml`
//! The class can be changed with the `class` property.
//! 
//! ### ID
//! This element is wrapped in a `<div>` whose `id` can be set with the `id` property.
//! 
//! ## RawSvg
//! 
//! ### Description
//! 
//! The `<RawSvg>` component is a Yew component that displays raw SVG.
//! 
//! Yew can accept `<svg>` tags (and descendants); however, `<RawSvg>` is needed
//! when a string containing SVG must be rendered; for example a SVG file loaded into
//! a String can be passed as `data` to `<RawSvg>`, and it will mount it and render it 
//! as if it were written plainly inside the `html!` macro.
//! 
//! Under the hood it uses the [`gloo_utils`] and [`VNode`].
//! 
//! > **Note**: The SVG String gets wrapped inide a `<div>`
//! 
//! ### Usage
//! 
//! The `<RawSvg>` component must be used in a Yew `html!` macro.
//! 
//! ```
//! html! {
//!     <RawSvg data={ r#"
//!         <svg height="130" width="130">
//!             <circle cx="45" cy="45" r="35" stroke="black" stroke-width="1" fill="blue" />
//!             Currently your browser does not support inline SVG.  
//!         </svg> 
//!     "#} />
//! };
//! ```
//! 
//! ### Properties
//! 
//! The component can accept the following poperties:
//! 
//! - `data`: `String` Prop to pass a Raw SVG String to <RawSvg> component
//! - `id:` `Option<String>` Element's ID (optional)
//! - `class`: `Option<String>` Element's CSS class; when specified, it replaces the default `rs-rawsvg`
//! 
//! ### CSS Class
//! This element is wrapped in a `<div>` styled with the class `rs-rawsvg`
//! The class can be changed with the `class` property.
//! 
//! ### ID
//! This element is wrapped in a `<div>` whose `id` can be set with the `id` property.
//! 
//! [`gloo_utils`]: https://docs.rs/gloo-utils/latest/gloo_utils/index.html
//! [`VNode`]: https://docs.rs/yew/latest/yew/virtual_dom/enum.VNode.html
use yew::{function_component, Html, Properties};

/// RawHtml component props
#[derive(Properties, PartialEq)]
pub struct RawHtmlProps {
    /// Prop to pass a Raw Html String to `<RawHtml>` component
    pub data: String,
    /// Element's ID (optional)
    #[prop_or_default]
    pub id: String,
    /// Element's CSS class; when specified, it replaces the default `rs-rawhtml`
    #[prop_or("rs-rawhtml".to_string())]
    pub class: String,
}

/// RawHtml component
#[function_component(RawHtml)]
pub fn raw_html(props: &RawHtmlProps) -> Html {
    let div = gloo_utils::document().create_element("div").unwrap();

    div.set_class_name(&props.class.as_str());
    div.set_id(&props.id.as_str());
    div.set_inner_html(&props.data.clone());

    Html::VRef(div.into())
}

/// RawSvg component props
#[derive(Properties, PartialEq)]
pub struct RawSvgProps {
    /// Prop to pass a Raw Svg String to `<RawSvg>` component
    pub data: String,
    /// Element's ID (optional)
    #[prop_or_default]
    pub id: String,
    /// Element's CSS class; when specified, it replaces the default `rs-rawsvg`
    #[prop_or("rs-rawsvg".to_string())]
    pub class: String,
}

/// RawSvg component
#[function_component(RawSvg)]
pub fn raw_html(props: &RawSvgProps) -> Html {
    let div = gloo_utils::document().create_element("div").unwrap();
    div.set_id(&props.id.as_str());
    div.set_class_name(&props.class.as_str());
    
    div.set_inner_html(&props.data.clone());
    Html::VRef(div.into())
}