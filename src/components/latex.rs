/* Roadmap:
- [x] Create branch for component
- [x] Make component
    - [x] modify mod.rs
    - [x] modify lib.rs
    - [x] modify list in lib.rs docs
    - [x] modify list in Readme
- [x] Document component
- [x] Example for component
    - [x] successfully run example
- [x] Finally, commit a finished element
- [ ] Improve the element, following the TODOs
*/
//! ## Description
//! 
//! The `<Latex>` component is a component that displays LaTeX math snippets. 
//! It is based on [`KaTeX`] and [`KaTeX-rs`].
//! 
//! For the `KaTeX` specific options, see [`KaTeX Options doc`].
//! 
//! ## Usage
//! 
//! The `<Latex>` component must be used in a Yew `html!` macro.
//! 
//! ```rust
//! use rustream::prelude::*;
//! 
//! fn main() {
//!     yew::start_app::<App>();
//! }
//! 
//! #[function_component(App)]
//! pub fn app() -> Html {
//!     let relativity = "E = mc^2".to_string();
//!     html! {
//!         <div>
//!             <h2>{ "Hello LaTeX" }</h2>
//!             <Latex data={ relativity } />
//!         </div>
//!     }
//! }
//! ```
//! 
//! The above requires the following dependencies in *Cargo.toml*
//! - yew
//! - rustream
//! - katex `default-features = false, features = ["wasm-js"]`
//! 
//! ### Usage caveat
//! 
//! The *index.html* needs to reference the [`KaTeX`]'s own CSS.
//! 
//! This can be achieved by adding the following link in the index's `<head>`
//! 
//! ```html
//! <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.16.0/dist/katex.min.css" integrity="sha384-Xi8rHCmBmhbuyyhbI88391ZKP2dmfnOl4rT9ZfRI7mLTdk1wblIUnrIq35nqwEvC" crossorigin="anonymous">
//! ```
//! 
//! In alternative, download and serve locally the minified `CSS` file.
//! 
//! For a discussion on fonts, see the [`KaTeX Fonts doc`].
//! 
//! ## Properties
//! 
//! The component can accept the following poperties:
//! 
//! - `data`: `String` Prop to pass a LaTeX String to <Latex> component
//! - `id:` `Option<String>` Element's ID (optional)
//! - `class`: `Option<String>` Element's CSS class; when specified, it replaces the default `rs-latex`
//! - `display`: `Option<bool>` KateX Options: displayMode; Disabled (false) by default.
//! 
//! For the `KaTeX` specific options, see [`KaTeX Options doc`].
//! 
//! ## CSS Class
//! This element is wrapped in a `<div>` styled with the class `rs-latex`
//! The class can be changed with the `class` property.
//! 
//! ## ID
//! This element is wrapped in a `<div>` whose `id` can be set with the `id` property.
//! 
//! [`KaTeX`]: https://katex.org
//! [`KaTeX-rs`]: https://crates.io/crates/katex
//! [`KaTeX Options doc`]: https://katex.org/docs/options.html
//! [`KaTeX Fonts doc`]: https://katex.org/docs/font.html
use yew::{function_component, html, Html, Properties};
use crate::prelude::*;
//use katex;

/// Latex component props
#[derive(Properties, PartialEq)]
pub struct LatexProps{
    /// Prop to pass a Latex String to `<Latex>` component
    pub data: String,
    /// Element's ID (optional)
    #[prop_or_default]
    pub id: String,
    /// Element's CSS class, to replace the default `rs-latex`
    #[prop_or("rs-latex".to_string())]
    pub class: String,

    /// KateX Options: displayMode; Disabled (false) by default.
    pub display: Option<bool>, 
     
} // @TODO: maybe change to give the ability to output in some way other than HTML? see https://katex.org/docs/options.html

/// Latex component
#[function_component(Latex)]
pub fn latex(props: &LatexProps) -> Html {
    // katex init
    use katex::Opts;
    let mut opts = Opts::builder();

    // Set options
    if props.display == Some(true) { opts.display_mode(true); } else { opts.display_mode(false); }
    //@TODO: maybe change to give the ability to output in some way other than HTML? see https://katex.org/docs/options.html
    opts.output_type(katex::OutputType::Html); // Forced by default

    let opts = opts.build().expect("KaTeX builder error");

    // katex render to String
    let html_output = katex::render_with_opts(&props.data.as_str(), &opts).expect("KaTeX rendering error");
    html! {
        <RawHtml data={ html_output } class={ props.class.clone() } id={ props.id.clone() } />
    }
}