/* Roadmap:
- [x] Create branch for component
- [x] Make component
    - [x] modify mod.rs
    - [x] modify lib.rs
    - [x] modify list in lib.rs docs
    - [x] modify list in Readme
- [x] Document component
- [x] Example for component
    - [x] successfully run example
- [x] Finally, commit a finished element
- [ ] Improve the element, following the TODOs
*/
// TODO: autogenerate ID for canvas thorugh uuid if not provided
//! # Description
//! 
//! This contains various components useful to create a HTML Canvas and draw in it.
//! 
//! ## Canvas
//! 
//! The `<Canvas>` component is a Yew component that inserts a <canvas> element in the page.
//! The element is a context that supports <CanvasElment> elements (see below)
//! Under the hood it uses a <RawCanvas> component.
//! 
//! ### Usage
//! 
//! The `<Canvas>` component must be used in a Yew `html!` macro.
//! 
//! ```
//! html! {
//!     <Canvas></Canvas>
//! };
//! ```
//! 
//! ### Properties
//! 
//! The component can accept the following poperties:
//! 
//! - `id:` `String` Element's ID (optional)
//! - `class`: `String` Element's CSS class; when specified, it replaces the default `rs-canvas`
//! - `width`: String Element's width, espressed either as pixel number or as percentage of the parents' size
//! - `height`: String Element's height, espressed either as pixel number or as percentage of the parents' size
//! 
//! #### CSS Class
//! This element wrapps a `<canvas>` styled with the class `rs-canvas`
//! The class can be changed with the `class` property.
//! 
//! #### ID
//! This element is wrapps a `<canvas>` whose `id` can be set with the `id` property.
//! 
//! #### Width
//! This element is wrapps a `<canvas>` whose `width` can be set with the `width` property.
//! 
//! #### Height
//! This element is wrapps a `<canvas>` whose `height` can be set with the `height` property.
//! 
//! ## CanvasElement
//! 
//! The `<CanvasElement>` component is a Yew component that inserts a drawable element 
//! in a <Canvas> component.
//! The element can consume the <Canvas> context to fetch the canvas' id.
//! 
//! ### Usage
//! 
//! The `<CanvasElement>` component must be used in a Yew `html!` macro,
//! inside the <Canvas></Canvas> tags.
//! 
//! ```
//! html! {
//!     <Canvas>
//!         <CanvasElement />
//!     </Canvas>
//! };
//! ```
//! 
//! ### Properties
//! 
//! The component can accept the following poperties:
//! 
//! - `callback`: a function or a closure that draws 
//! 
//! #### Callback
//! This element accepts a function callback (`yew::callback::Callback;`) with a `String` parameter, to pass the Canvas' id to it
//! The drawing logic may be placed inside the callback
//! 
//! See the [canvas](https://codeberg.org/davidedelpapa/rustream/src/branch/main/examples/canvas) and [plotters-integration](https://codeberg.org/davidedelpapa/rustream/src/branch/main/examples/plotters-integration) examples.
use yew::{ContextProvider, Children, function_component, html, Html, Properties, use_state};
use yew::callback::Callback;
use crate::prelude::*;

/// Canvas component props
#[derive(Properties, PartialEq)]
pub struct CanvasProps {
    #[doc(hidden)]
    #[prop_or_default]
    pub children: Children,
    /// Element's ID (optional)
    #[prop_or("rs-canvas".to_string())]
    pub id: String,
    /// Element's CSS class; when specified, it replaces the default `rs-canvas`
    #[prop_or("rs-canvas".to_string())]
    pub class: String,
    /// Element's width (String): either a px number or a percentage) (optional)
    #[prop_or("320".to_string())]
    pub width: String,
    /// Element's height (String): either a px number or a percentage) (optional)
    #[prop_or("200".to_string())]
    pub height: String,
    /// Fallback Html for when the canvas' chidren are drawing.
    /// It defaults to `html! {<div>{"Loading..."}</div>}`
    #[prop_or(html! {<div>{"Loading..."}</div>})]
    pub fallback: Html,
}

/// Canvas context
/// 
/// Can be used by any of the children of a `<Canvas>` component wth `use_context`
/// 
/// ## Usage
/// 
/// ```rust
/// let ctx = use_context::<CanvasContext>().expect("no ctx found");
/// ```
// @TODO: if id is empty create uuid; 
//        for now there is one fixed, but may cause confolict for multiple canvases in page
#[derive(Debug, Clone, PartialEq)]
pub struct CanvasContext {
    /// This context contains the canvas' ID
    pub id: String,
}

/// RawCanvas component props
#[derive(Properties, PartialEq)]
pub struct RawCanvasProps {
    /// Element's ID (optional)
    #[prop_or_default]
    pub id: String,
    /// Element's CSS class; when specified, it replaces the default `rs-rawcanvas`
    #[prop_or("rs-rawcanvas".to_string())]
    pub class: String,
    /// Element's width (String): either a px number or a percentage) (optional)
    #[prop_or("320".to_string())]
    pub width: String,
    /// Element's height (String): either a px number or a percentage) (optional)
    #[prop_or("200".to_string())]
    pub height: String,
}

/// RawCanvas component
#[function_component(RawCanvas)]
pub fn raw_canvas(props: &RawCanvasProps) -> Html {
    let canvas = gloo_utils::document().create_element("canvas").unwrap();
    canvas.set_id(props.id.as_str());
    canvas.set_class_name(props.class.as_str());
    let _ = canvas.set_attribute("width", props.width.as_str());
    let _ = canvas.set_attribute("height", props.height.as_str());
    Html::VRef(canvas.into())
}

/// Canvas component
#[function_component(Canvas)]
pub fn canvas(props: &CanvasProps) -> Html {
    let ctx = use_state(|| CanvasContext {
        id: props.id.clone(),
    });
    let fallback = props.fallback.clone();
    html! {
        <>
                       
            <RawCanvas id={props.id.clone()} class={props.class.clone()} width={props.width.clone()} height={props.height.clone()}/>
            <Suspense {fallback}>
                <ContextProvider<CanvasContext> context={(*ctx).clone()}>
                    { for props.children.iter() }
                </ContextProvider<CanvasContext>>
            </Suspense>
            
        </>
    }
}

/// CanvasElement component props
#[derive(Properties, PartialEq)]
pub struct CanvasElementProps {
    /// Function to draw on the canvas.
    /// It gets called with the canvas' ID for quick recognition
    #[prop_or_default]
    pub callback: Callback<String>,
}

/// CanvasElement component
#[function_component(CanvasElement)]
pub fn canvas_element(props: &CanvasElementProps) -> Html {
    let canvas_id = use_context::<CanvasContext>().expect("no CanvasContext found").id;
    props.callback.emit(canvas_id);
    html! {}
}