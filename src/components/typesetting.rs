/* Roadmap:
- [ ] Create branch for component <-- FORGOT!!!
- [x] Make component
    - [x] modify mod.rs
    - [x] modify lib.rs
    - [x] modify list in lib.rs docs
    - [x] modify list in Readme
- [x] Document component
- [x] Example for component
    - [x] successfully run example
- [x] Finally, commit a finished element
- [ ] Improve the element, following the TODOs
*/
// TODO: Style the title!!! Right now it is less significant than an H1
//! # Description
//! 
//! This contains various components useful for formatting a web page.
//! 
//! ## Title
//! 
//! The `<Title>` element creates a `<header>` element
//! 
//! ### Usage
//! 
//! The `<Title>` component must be used in a Yew `html!` macro.
//! 
//! ```
//! html! {
//!     <Title data={"Foo".to_string()} />
//! };
//! ```
//! 
//! ### Properties
//! 
//! The component can accept the following poperties:
//! 
//! - `data`: `String` Prop to pass a Raw Html String to <Title> component
//! - `id:` `Option<String>` Element's ID (optional)
//! - `class`: `Option<String>` Element's CSS class; when specified, it replaces the default `rs-title`
//! - `changehead`: `Option<bool>` When `false`, the element does not change the `<title>`content inside the `<head>` of the document. Default: `true`.
//! ### CSS Class
//! This element is wrapped in a `<header>` styled with the class `rs-title`
//! The class can be changed with the `class` property.
//! 
//! ### ID
//! This element is wrapped in a `<header>` whose `id` can be set with the `id` property.
//! 
//! ## Header
//! 
//! The `<header>` element creates a `<h1>` element
//! 
//! ### Usage
//! 
//! The `<Header>` component must be used in a Yew `html!` macro.
//! 
//! When used in a `<Page>` element it gives the possibility to automatically fill 
//! the content of a `<Toc>` component.
//! 
//! > *Note*: when the `<Header>` elements have an `id` set, the `<Toc>` also creates a link to the header.
//! 
//! ```
//! html! {
//!     <Header data={"Foo".to_string()} />
//! };
//! ```
//! 
//! ### Properties
//! 
//! The component can accept the following poperties:
//! 
//! - `data`: `String` Prop to pass a Raw Html String to <Header> component
//! - `id:` `Option<String>` Element's ID (optional)
//! - `class`: `Option<String>` Element's CSS class; when specified, it replaces the default `rs-header`
//! 
//! ### CSS Class
//! This element is wrapped in a `<h1>` styled with the class `rs-title`
//! The class can be changed with the `class` property.
//! 
//! ### ID
//! This element is wrapped in a `<h1>` whose `id` can be set with the `id` property.
//! 
// @TODO add a default css (that can be overridden, to load with the page
use yew::{function_component, html, Html, Properties, use_context, Component, Context};
use crate::{components::layout::{PageContext, TOCEntry}, PageAction};

/// Title component props
#[derive(Properties, PartialEq)]
pub struct TitleProps{
    /// Prop to pass a String to the `<Title>` component
    pub data: String,
    /// Element's ID (optional)
    #[prop_or_default]
    pub id: String,
    /// Element's CSS class, to replace the default `rs-title`
    #[prop_or("rs-title".to_string())]
    pub class: String,

    /// When `false`, the element does not change the `<title>`content inside the `<head>` of the document.
    /// Default: `true`.
    pub changehead: Option<bool>,
}

/// Title component
#[function_component(Title)]
pub fn title(props: &TitleProps) -> Html {
    let context = use_context::<PageContext>();

    // This changes the `<title>` inside the `<head>` of the document
    if !(props.changehead == Some(false)) {
        gloo_utils::document().set_title(&props.data.clone());
    }

    html!{
        <TitleInner data={props.data.clone()} class={ props.class.clone() } 
            id={ props.id.clone() } changehead={props.changehead.clone()} {context} />
    }
}

#[doc(hidden)]
#[derive(Properties, PartialEq)]
pub struct TitleInnerProps{
    #[doc(hidden)]
    pub data: String,
    #[doc(hidden)]
    #[prop_or_default]
    pub id: String,
    #[doc(hidden)]
    #[prop_or("rs-title".to_string())]
    pub class: String,
    #[doc(hidden)]
    pub changehead: Option<bool>,
    #[doc(hidden)]
    pub context: Option<PageContext>,
}

#[doc(hidden)]
pub struct TitleInner;
impl Component for TitleInner{
    type Message = ();
    type Properties = TitleInnerProps;

    fn create(ctx: &Context<Self>) -> Self {
        let context = &ctx.props().context;
        match context {
            Some(c) => {
                c.inner.dispatch(PageAction::SetPageTitle(ctx.props().data.clone()));
            },
            None => (),
        } 
        TitleInner
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let doc_title = ctx.props().data.clone();
        html!{
            <header class={ ctx.props().class.clone() } id={ ctx.props().id.clone() }>
                { doc_title }
            </header>
        }
    }    

}
/// Header component props
#[derive(Properties, PartialEq)]
pub struct HeaderProps{
    /// Prop to pass a String to the `<Title>` component
    pub data: String,
    /// Element's ID (optional)
    #[prop_or_default]
    pub id: String,
    /// Element's CSS class, to replace the default `rs-title`
    #[prop_or("rs-header".to_string())]
    pub class: String,
}

/// Header component
#[function_component(Header)]
pub fn header(props: &HeaderProps) -> Html {
    let context = use_context::<PageContext>();
    html! {<HeaderInner data={props.data.clone()} id={props.id.clone()} 
        class={props.class.clone()} {context} />}
}


#[doc(hidden)]
#[derive(Properties, PartialEq)]
pub struct HeaderInnerProps{
    #[doc(hidden)]
    pub data: String,
    #[doc(hidden)]
    #[prop_or_default]
    pub id: String,
    #[doc(hidden)]
    #[prop_or("rs-header".to_string())]
    pub class: String,
    #[doc(hidden)]
    pub context: Option<PageContext>,
}
#[doc(hidden)]
pub struct HeaderInner;
impl Component for HeaderInner {
    type Message = ();
    type Properties = HeaderInnerProps;

    fn create(ctx: &Context<Self>) -> Self {
        let context = &ctx.props().context;
        match context {
            Some(c) => {
                let header_title = ctx.props().data.clone();
                let id = ctx.props().id.clone();
                
                c.inner.dispatch(PageAction::SetToc(TOCEntry::new(
                        id.clone(),
                        header_title.clone(),
                        1
                )));
            },
            None => (),
        } 
        HeaderInner
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        html! {
            <div>
                <h1 class={ ctx.props().class.clone() } id={ ctx.props().id.clone() }>
                
                    { ctx.props().data.clone() }
                </h1>
            </div>
        }
    }

}